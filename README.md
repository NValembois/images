# kubitus-images

## Purpose

`Dockerfile`s for images used in `kubitus-installer`.

## Images

One flavor is available:

- `ca-certificates`
  - Based on `debian:bookworm`
  - Including package: `ca-certificates`, `ca-certificates-java`
  - Creating group and user `cacerts`, allowed to read and write `/etc/default/cacerts` and `/etc/ssl/certs/java/cacerts`
  - Running as `nobody` user


The image name is `registry.gitlab.com/kubitus-project/kubitus-images/${flavor}`.
For example, to retrieve the latest `ca-certificates` image:
`registry.gitlab.com/kubitus-project/kubitus-images/ca-certificates`.
